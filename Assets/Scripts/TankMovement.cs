﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 坦克移动控制 测试日志
 * 
 */


public class TankMovement : MonoBehaviour {
    public float speed = 5;
    public int number = 1; // 玩家编号
    public float angularSpeed = 30; 

    private Rigidbody rigibody;
	// Use this for initialization
	void Start () {
        rigibody = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        float v = Input.GetAxis("VerticalP" + number); //Y轴
        float h = Input.GetAxis("HorizontalP" + number); //X轴

        rigibody.velocity = transform.forward * v * speed; //给刚体添加一个速度
        rigibody.angularVelocity = transform.up * h * angularSpeed; //给刚体的Y轴添加一个角速度
	}
}
